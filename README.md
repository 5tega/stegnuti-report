# Stegnuti Report

Runtime testing and debugging tool for Unity applications. Helps you collect data about how your game is being played and if any bugs occur. Useful when in closed testing.
